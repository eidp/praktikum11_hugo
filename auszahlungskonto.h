#include "bankkonto.h"

#ifndef CPP_SUCKS_AUSZAHLUNGSKONTO
#define CPP_SUCKS_AUSZAHLUNGSKONTO

class Auszahlungskonto : public Bankkonto {
    protected:
        int auszahlen(int betrag, int a) {
            if (betrag + a <= aktuellerKontostand) {
                aktuellerKontostand -= betrag;
                return betrag;
            }
            aktuellerKontostand = a;
            return aktuellerKontostand - a;
        }
};

#endif