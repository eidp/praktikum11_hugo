#include "auszahlungskonto.h"
#include "bankkonto.h"

#ifndef CPP_SUCKS_GIROKONTO
#define CPP_SUCKS_GIROKONTO

class Girokonto : public Auszahlungskonto {
    public:
        int ueberweisen(Bankkonto& zielKonto, int betrag) {
            int geld = auszahlen(betrag, -1000 * 100); // Euro -> Cent
            zielKonto.gutschreiben(geld);
            return geld;
        }

        int auszahlen(int betrag) {
            return auszahlen(betrag, -1000 * 100); // Euro -> Cent
        }
};

#endif