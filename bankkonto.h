

#ifndef CPP_SUCKS_BANKKONTO
#define CPP_SUCKS_BANKKONTO

#include <string>

using namespace std;

class Bankkonto {
    private:
        unsigned long kontonummer;
        string inhaber;
    
    protected:
        int aktuellerKontostand;

    public:
        Bankkonto() {
            aktuellerKontostand = 0;
        }

        void gutschreiben(int betrag) {
            if (betrag < 0) {
                cout << "Betrag ist negativ." << endl;
                return;
            }
            aktuellerKontostand += betrag;
        }
        int kontostand () {
            return aktuellerKontostand;
        }
        void anzeigen() const {
            cout<< "Kontonummer: " << kontonummmer << endl;
            cout << "Inhaber: " << inhaber << endl;
            cout << "Aktueller Kontostand: " << aktuellerKontostand << endl;
        }
        unsigned long kontonummer() const {
            return kontonummer;
        }
};

#endif